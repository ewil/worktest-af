﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TravelWeather.Api.Models;

namespace TravelWeather.Services
{
    public interface ITrafiklabService
    {
        Task<List<CityModel>> GetCities(string cityName);
        Task<TripResultModel> GetTrip(int toCityId);
    }
}
