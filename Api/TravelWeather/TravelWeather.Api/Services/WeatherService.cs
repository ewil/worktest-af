﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using TravelWeather.Api.Models;

namespace TravelWeather.Services
{
    public class WeatherService : IWeatherService
    {
        private readonly string _smhiApiUrl = ConfigurationManager.AppSettings["SmhiApiUrl"];
        
        public async Task<WeatherForcastModel> GetForcast(double lon, double lat, DateTime date)
        {
            WeatherForcastModel weatherForcastModel;
            HttpResponseMessage response;
            string _lon = lon.ToString().Replace(",", ".");
            string _lat = lat.ToString().Replace(",", ".");

            using (HttpClient client = new HttpClient())
            {
                string requstUrl = $@"{_smhiApiUrl}geotype/point/lon/{_lon}/lat/{_lat}/data.json";
                response = await client.GetAsync(requstUrl);
            }

            if (response.IsSuccessStatusCode)
            {
                JObject json = await response.Content.ReadAsAsync<JObject>();
                bool forcastFoun = json.TryGetValue("timeSeries", out JToken jToken);
                if (forcastFoun)
                {
                    DateTime arivalTimeAsUtc = date.ToUniversalTime();
                    TimeSpan setTime;
                    if (arivalTimeAsUtc.Minute > 30)
                    {
                        setTime = new TimeSpan(arivalTimeAsUtc.Hour + 1, 0, 0);
                    }
                    else
                    {
                        setTime = new TimeSpan(arivalTimeAsUtc.Hour, 0, 0);
                    }
                    arivalTimeAsUtc = arivalTimeAsUtc.Date + setTime;
                    string arivalTimeAsString = arivalTimeAsUtc.ToString("yyyy-MM-dd HH:mm:ss");
                    JObject selectedTime = jToken.Children<JObject>().Where(w => w["validTime"].ToString() == arivalTimeAsString).FirstOrDefault();

                    if (selectedTime == null)
                    {
                        selectedTime = jToken.Children<JObject>().FirstOrDefault();
                    }
                    JToken weather = selectedTime["parameters"];
                    weatherForcastModel = new WeatherForcastModel
                    {
                        Temp = (double)weather.Where(w => (string)w["name"] == "t").FirstOrDefault()["values"].First, //c
                        WindDirection = (int)weather.Where(w => (string)w["name"] == "wd").FirstOrDefault()["values"].First, //degree
                        WindSpeed = (double)weather.Where(w => (string)w["name"] == "ws").FirstOrDefault()["values"].First, //m/s
                        WeatherSymbol = (int)weather.Where(w => (string)w["name"] == "Wsymb2").FirstOrDefault()["values"].First, //0-27
                        ForcastTime = DateTime.Parse((string)selectedTime["validTime"] + "Z")
                    };
                    //eatherForcastModel.ForcastTime.Kind = DateTimeKind.Utc;
                    return weatherForcastModel;
                }
            }
            return null;
        } 
    }
}