﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using TravelWeather.Api.Models;

namespace TravelWeather.Services
{
    public class TrafiklabService : ITrafiklabService
    {
        private readonly string _resBotApiUrl = ConfigurationManager.AppSettings["ResRobotApiUrl"];
        private readonly string _resBotApiKey = ConfigurationManager.AppSettings["ResRobotApiKey"];
        private readonly int _orebroCentralStationId = 740000133;

        public async Task<List<CityModel>> GetCities(string cityName)
        {
            List<CityModel> cityModelList = new List<CityModel>();
            cityName = HttpUtility.UrlEncode(cityName);
            HttpResponseMessage response;

            using (HttpClient client = new HttpClient())
            {
                string requstUrl = $@"{_resBotApiUrl}location.name?key={_resBotApiKey}&input={cityName}?&format=json";
                response = await client.GetAsync(requstUrl);
            }

            if (response != null && response.IsSuccessStatusCode)
            {
                JObject json = await response.Content.ReadAsAsync<JObject>();
                bool locationFoun = json.TryGetValue("StopLocation", out JToken jToken);

                if (locationFoun)
                {
                    foreach (var item in jToken.Children<JObject>())
                    {
                        CityModel cityModel = new CityModel();
                        cityModel.Name = (string)item["name"];
                        cityModel.Id = (int)item["id"];
                        cityModelList.Add(cityModel);
                    }
                }
            }
           
            return cityModelList;
        }

        public async Task<TripResultModel> GetTrip(int toCityId)
        {
            TripResultModel tripResult = new TripResultModel();
            HttpResponseMessage response;

            using (HttpClient client = new HttpClient())
            {
                string requstUrl = $@"{_resBotApiUrl}trip?key={_resBotApiKey}&originId={_orebroCentralStationId}&destId={toCityId}&format=json";
                response = await client.GetAsync(requstUrl);
            }

            if (response != null && response.IsSuccessStatusCode)
            {
                JObject json = await response.Content.ReadAsAsync<JObject>();
                bool tripFound = json.TryGetValue("Trip", out JToken jToken);
                if (tripFound)
                {
                    JToken tripDestination = jToken.First["LegList"]["Leg"].Last["Destination"];
                    DateTime arivalTime = DateTime.Parse((string)tripDestination["date"] + " " + (string)tripDestination["time"]);

                    tripResult.Destination = new DestinationModel
                    {
                        ArivalTime = arivalTime,
                        Lon = (double)tripDestination["lon"],
                        Lat = (double)tripDestination["lat"],
                        Name = (string)tripDestination["name"]
                    };

                    JToken tripOrigin = jToken.First["LegList"]["Leg"].First["Origin"];
                    tripResult.Origin = new OriginModel
                    {
                        StartTime = DateTime.Parse((string)tripOrigin["date"] + " " + (string)tripOrigin["time"])
                    };

                    return tripResult;
                }
            }
            return null;
        }
    }
}