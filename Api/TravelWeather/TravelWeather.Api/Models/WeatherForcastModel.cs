﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelWeather.Api.Models
{
    public class WeatherForcastModel
    {
        public double Temp { get; set; }
        public int WindDirection { get; set; }
        public double WindSpeed { get; set; }
        public int WeatherSymbol { get; set; }
        public DateTime ForcastTime { get; set; }
    }
}