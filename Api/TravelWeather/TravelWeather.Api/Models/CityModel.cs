﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelWeather.Api.Models
{
    public class CityModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}