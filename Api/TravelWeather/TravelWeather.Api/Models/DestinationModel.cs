﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelWeather.Api.Models
{
    public class DestinationModel
    {
        public string Name { get; set; }
        public DateTime ArivalTime { get; set; }
        public double Lon { get; set; }
        public double Lat { get; set; }
    }
}