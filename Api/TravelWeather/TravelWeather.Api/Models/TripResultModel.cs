﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelWeather.Api.Models
{
    public class TripResultModel
    {
        public OriginModel Origin { get; set; }
        public DestinationModel Destination { get; set; }
        public WeatherForcastModel WeatherForcast { get; set; }
    }
}