﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using TravelWeather.Api.Models;
using TravelWeather.Services;

namespace TravelWeather.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/City")]
    public class CityController : ApiController
    {
        ITrafiklabService trafiklabService = new TrafiklabService();
        
        [Route("{name}")]
        public async Task<IHttpActionResult> Get([FromUri]string name)
        {
            List<CityModel> model = await trafiklabService.GetCities(name);
            return Json(model);
        }
    }
}
