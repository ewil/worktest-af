﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using TravelWeather.Api.Models;
using TravelWeather.Services;

namespace TravelWeather.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/Trip")]
    public class TripController : ApiController
    {
        private readonly ITrafiklabService trafiklabService = new TrafiklabService();
        private readonly IWeatherService weatherService = new WeatherService();

        [Route("{id}")]
        public async Task<IHttpActionResult> Get([FromUri]int id)
        {
            TripResultModel model = new TripResultModel();
            model = await trafiklabService.GetTrip(id);
            model.WeatherForcast = await weatherService.GetForcast(model.Destination.Lon, model.Destination.Lat, model.Destination.ArivalTime);
            return Json(model);
        }
    }
}
