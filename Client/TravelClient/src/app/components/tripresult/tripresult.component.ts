import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { ActivatedRoute } from '@angular/router';
import { TripResult } from '../../models/travelmodels';

@Component({
  selector: 'app-tripresult',
  templateUrl: './tripresult.component.html',
  styleUrls: ['./tripresult.component.css']
})
export class TripresultComponent implements OnInit {
  cityId: number;
  trip: TripResult;
  winddirection: string;
  weatherPhrase: string;
  weatherPhrases: string[];
  constructor(private api: ApiService, private route: ActivatedRoute) {
    this.trip = null;
    this.winddirection = '';
    this.weatherPhrases = ['Clear sky', 'Nearly clear sky', 'Variable cloudiness', 'Halfclear sky', 'Cloudy sky', 'Overcast', 'Fog',
      'Light rain showers', 'Moderate rain showers', 'Heavy rain showers', 'Thunderstorm', 'Light sleet showers', 'Moderate sleet showers',
      'Heavy sleet showers', 'Light snow showers', 'Moderate snow showers', 'Heavy snow showers', 'Light rain', 'Moderate rain',
      'Heavy rain', 'Thunder', 'Light sleet', 'Moderate sleet', 'Heavy sleet', 'Light snowfall', 'Moderate snowfall', 'Heavy snowfall'];
    this.route.params.subscribe(params => this.cityId = params.id);
  }

  ngOnInit() {
    this.api.getTrip(this.cityId).subscribe(result => this.trip = result,
      error => console.error(error),
      () => this.createView());
  }

  createView() {
    this.winddirection = `rotate(${this.trip.WeatherForcast.WindDirection}deg)`;
    this.trip.Destination.ArivalTime = this.trip.Destination.ArivalTime.replace(/T/gi, ' ').slice(0, -3);
    this.trip.Origin.StartTime = this.trip.Origin.StartTime.replace(/T/gi, ' ').slice(0, -3);
    this.weatherPhrase = this.weatherPhrases[this.trip.WeatherForcast.WeatherSymbol - 1];
  }
}
