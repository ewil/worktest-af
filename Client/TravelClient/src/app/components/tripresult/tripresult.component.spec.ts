import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TripresultComponent } from './tripresult.component';

describe('TripresultComponent', () => {
  let component: TripresultComponent;
  let fixture: ComponentFixture<TripresultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TripresultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TripresultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
