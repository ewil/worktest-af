import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { CityList } from '../../models/travelmodels';

@Component({
  selector: 'app-cityselect',
  templateUrl: './cityselect.component.html',
  styleUrls: ['./cityselect.component.css']
})
export class CityselectComponent implements OnInit {
  cities: CityList;
  constructor(private api: ApiService) {
    this.cities = null;
  }

  ngOnInit() {
  }

  searchField(event: KeyboardEvent) {
    const searchInput = (<HTMLInputElement>event.target).value;
    if (searchInput.length > 2) {
      console.log(searchInput);
      this.api.getCities(searchInput).subscribe(response => this.cities = response,
        error => console.log(error));
    } else if (searchInput.length === 0) {
      this.cities = null;
    }
  }

}
