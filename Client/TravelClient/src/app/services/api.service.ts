import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CityList, TripResult } from '../models/travelmodels';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = 'http://localhost:52385/api/';
  }

  getCities(name: string) {
    const uri = `${this.baseUrl}city/${name}`;
    return this.http.get<CityList>(uri);
  }

  getTrip(id: number) {
    const uri = `${this.baseUrl}trip/${id}`;
    return this.http.get<TripResult>(uri);
  }
}
