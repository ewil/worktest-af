export interface City {
  Id: number;
  Name: string;
}
export interface CityList {
  Cities: City[];
}

export interface Origin {
  StartTime: string;
}

export interface Destination {
  Name: string;
  ArivalTime: string;
}

export interface WeatherForcast {
  Temp: number;
  WindDirection: number;
  WindSpeed: number;
  WeatherSymbol: number;
  ForcastTime: string;
}

export interface TripResult {
  Origin: Origin;
  Destination: Destination;
  WeatherForcast: WeatherForcast;
}
