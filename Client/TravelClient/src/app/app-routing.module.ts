import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CityselectComponent } from './components/cityselect/cityselect.component';
import { TripresultComponent } from './components/tripresult/tripresult.component';

const routes: Routes = [{
  path: '',
  component: CityselectComponent
}, {
  path: 'result/:id',
  component: TripresultComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
